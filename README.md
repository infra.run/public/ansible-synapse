matrix-synapse
==============



Role dependencies
-----------------

- https://gitlab.com/infra.run/public/ansible-base.git
- https://gitlab.com/infra.run/public/ansible-nginx.git
- https://gitlab.com/infra.run/public/ansible-dehydrated.git
- https://gitlab.com/infra.run/public/ansible-postgres.git

Links
-----

- https://github.com/UdelaRInterior/ansible-role-matrix-synapse/blob/master/tasks/install_from_source.yml
- https://github.com/matrix-org/matrix-ansible-synapse/blob/master/tasks/systemd.yml